#ifndef __MEM_H__
#define __MEM_H__

#include <stdio.h>
#include "common.h"

#ifdef MEM_DEBUG

#ifdef DEBUG_FILE
extern FILE *f;
#define MEM_DBG(fmt, ...)   fprintf(f, fmt, __VA_ARGS__);
#else
#define MEM_DBG(fmt, ...)   printf(fmt, __VA_ARGS__);
#endif

#else
#define MEM_DBG(fmt, ...)
#endif

struct nes_cart {
    u8 *prg_rom;
    u8 *chr_rom;
    u8 *prg_ram;
    u8 prg_rom_sz;
    u8 chr_rom_sz;
    u8 prg_ram_sz;
    int mapper_nr;
    int mirroring;
};

int mem_init(struct nes_cart *cart);

void mem_store(u16 address, u8 value);
u8 mem_load(u16 address);
/* does not change registers */
u8 discr_mem_load(u16 address);

void mem_store_ppu(u16 address, u8 value);
u8 mem_load_ppu(u16 address);
int ppu_get_chrom(u8 **chr);
int ppu_get_vram(u8 **ram);
int ppu_get_OAM(u8 **oam);
#endif // __MEM_H__
