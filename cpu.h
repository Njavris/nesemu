#ifndef __CPU_H__
#define __CPU_H__

#include <stdio.h>
#include "common.h"

#define STACK_BASE  0x100 // page 1(0x0100 - 0x01ff)moving down
#define NMI_VEC_1   0xfffa
#define NMI_VEC_0   (NMI_VEC_1 + 1)
#define RST_VEC_1   0xfffc
#define RST_VEC_0   (RST_VEC_1 + 1)
#define IRQ_VEC_1   0xfffe
#define IRQ_VEC_0   (IRQ_VEC_1 + 1)
#define NOP         0xea

#ifdef CPU_DEBUG



#ifdef DEBUG_FILE
extern FILE *f;
#define CPU_DBG(fmt, ...)   fprintf(f, fmt, __VA_ARGS__);
#else
#define CPU_DBG(fmt, ...)   printf(fmt, __VA_ARGS__);
#endif

#else
#define CPU_DBG(fmt, ...)
#endif

enum opcode_mode {
    ABSOLUTE,
    ABSOLUTE_X,
    ABSOLUTE_Y,
    ACCUMULATOR,
    IMMEDIATE,
    IMPLIED,
    INDIRECT,
    INDIRECT_X,
    INDIRECT_Y,
    RELATIVE,
    ZERO_PAGE,
    ZERO_PAGE_X,
    ZERO_PAGE_Y
};

struct cpu_state {
    u16 PC; // program counter
    u8 SP; // stack pointer
    union {
        struct {
            unsigned int C:1; // carry
            unsigned int Z:1; // zero
            unsigned int I:1; // interrupt
            unsigned int D:1; // decimal(unused, might as well skip)
            unsigned int B:1; // break
            unsigned int unused:1;
            unsigned int V:1; // overflow (value < -0x80) || (value > 0x80) meaning that value overflows and changes sign
            unsigned int N:1; // negative
        };
        u8 flags;
    } SR; // processor status
    u8 A; // accumulator
    u8 X; // index x
    u8 Y; // index y
    int cycles_inactive;
    u8 curr_opcode;
    struct cpu_opcode *curr_op;
    union {
        u8 par[2];
        u16 address;
    } curr_operand; // bytecode from program
    u16 curr_operand_value;  // value
    u16 curr_operand_address; // load/store address
    u8 curr_opcode_mode; // addressing mode
    int curr_op_length;
    u8 reloc;

    /*
        On power up:
        A = X = Y = 0
        SP = 0xfd
        SR = 0x22;

        States:
        Fetch op;
        Decode;
        Fetch data;
        Execute;
        Wait cycles;
    */
} MOS6502_state;

struct cpu_opcode {
    u8 opcode;
    char *name; // for disassembler
    int cycles;
    u8 access_cycle;
    u8 mode;
    void (*op_func)(void);
};

#define DUMP_STATE   CPU_DBG("PC: %04x - %s %04x\n"             \
                             "SR: %02x SP: %02x\n"              \
                             "A: %02x X: %02x Y: %02x\n"        \
                             "C:%d Z:%d I:%d D:%d B:%d U:%d V:%d N:%d\n" \
                        "curr_val: %04x curr_addr: %04x\n"      \
                        "cycles: %d len: %d\n\n",               \
    MOS6502_state.PC, MOS6502_state.curr_op->name,              \
    MOS6502_state.curr_operand.address, MOS6502_state.SR.flags, \
    MOS6502_state.SP, MOS6502_state.A, MOS6502_state.X,         \
    MOS6502_state.Y, MOS6502_state.SR.C, MOS6502_state.SR.Z ,   \
    MOS6502_state.SR.I, MOS6502_state.SR.D, MOS6502_state.SR.B, \
    MOS6502_state.SR.unused, MOS6502_state.SR.V,                \
    MOS6502_state.SR.N, MOS6502_state.curr_operand_value,       \
    MOS6502_state.curr_operand_address,                         \
    MOS6502_state.cycles_inactive, MOS6502_state.curr_op_length);

void interrupt_request(void);
void non_maskable_interrupt(void);
void init_cpu(void);
void cpu_cycle(void);

#endif
