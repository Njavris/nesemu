#ifndef __WINDOW_H__
#define __WINDOW_H__

#include <stdlib.h>
#include <stdio.h>
#include <SDL.h>
#include <SDL_syswm.h>
#include "common.h"
//http://lazyfoo.net/tutorials/SDL/01_hello_SDL/index2.php

void *spawn_window(int width, int height, int scaling, char *name);
int close_window(void* instance);
int put_screen(void* instance, u32 *frameBuf, int len);

#endif // __WINDOW_H__
