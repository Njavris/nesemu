#ifndef __PPU_H__
#define __PPU_H__

#include <stdio.h>
#include "common.h"

#define PPU_SCREEN_WIDTH    256
#define PPU_SCREEN_HEIGHT   240
#define PPU_PIXEL_COUNT     (PPU_SCREEN_WIDTH * PPU_SCREEN_HEIGHT)

#ifdef PPU_DEBUG

#ifdef DEBUG_FILE
extern FILE *f;

#define PPU_DBG(fmt, ...)   fprintf(f, fmt, __VA_ARGS__);
#else
#define PPU_DBG(fmt, ...)   printf(fmt, __VA_ARGS__);
#endif

#else
#define PPU_DBG(fmt, ...)
#endif



u8 read_ppu_reg(u8 reg);
void write_ppu_reg(u8 reg, u8 val);
u32 *get_framebuffer(void);
u32 *get_nametable(void);
int init_ppu(void);
void draw_pattern_tables(u32 *dst);
void draw_name_tables(void);
int ppu_render(void);
int nmi_set(void);
void set_vblank(u8 val);
#endif // __PPU_H__
