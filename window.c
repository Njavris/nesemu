#include "window.h"
#include <SDL.h>

#define AMASK   (0xff < 0x18)
#define BMASK   (0xff < 0x10)
#define GMASK   (0xff < 0x8)
#define RMASK   (0xff)

struct graphics {
    SDL_Window *window;
    SDL_Surface* screenSurface;
    SDL_Event event;
    int screen_width;
    int screen_height;
    int screen_scale;
};

void *spawn_window(int width, int height, int scaling, char *name)
{
    struct graphics *SDL_state = (struct graphics*)malloc(sizeof(struct graphics));
    SDL_state->screen_width = width;
    SDL_state->screen_height = height;
    SDL_state->screen_scale = scaling;

    if( SDL_Init( SDL_INIT_VIDEO ) < 0 ) {
        printf("SDL init failed: %s\n", SDL_GetError());
        return 0;
    }

    SDL_state->window =
            SDL_CreateWindow(name,
                             SDL_WINDOWPOS_UNDEFINED,
                             SDL_WINDOWPOS_UNDEFINED,
                             SDL_state->screen_width * SDL_state->screen_scale,
                             SDL_state->screen_height * SDL_state->screen_scale,
                             SDL_WINDOW_SHOWN);
    if (!SDL_state->window) {
        printf("SDL window creation failed: %s\n", SDL_GetError());
        return 0;
    }

    SDL_state->screenSurface = SDL_GetWindowSurface(SDL_state->window);
    SDL_UpdateWindowSurface(SDL_state->window);
    return SDL_state;
}

static void scaled_2d_memcpy(struct graphics *SDL_state, u32 *dst, u32 *src, int len, int scale) {
    int x, y, scx, scy;
    scx = scy = scale;
    if (scale == 0 || scale == 1)
        SDL_memcpy(dst, src, len * sizeof(u32));
    else {
        for (y = 0; y < SDL_state->screen_height; y++) {
            for (scy = 0; scy < scale; scy++) {
                for (x = 0; x < SDL_state->screen_width; x++) {
                    for (scx = 0; scx < scale; scx++)
                        dst[(y * scale + scy) * SDL_state->screen_width * scale + x * scale + scx]
                            = src[y * SDL_state->screen_width + x];
                }
            }
        }
    }
}

int put_screen(void *instance, u32 *frameBuf, int len)
{
    struct graphics *SDL_state = (struct graphics*)instance;
    while(SDL_PollEvent(&SDL_state->event)) {
        if (SDL_state->event.type == SDL_QUIT) {
            close_window(SDL_state);
            exit(0);
        }
    }
    SDL_LockSurface(SDL_state->screenSurface);
    scaled_2d_memcpy(SDL_state,
                     SDL_state->screenSurface->pixels,
                     frameBuf,
                     len,
                     SDL_state->screen_scale);
    SDL_UnlockSurface(SDL_state->screenSurface);
    SDL_UpdateWindowSurface(SDL_state->window);
    return 0;
}

int close_window(void *instance)
{
    struct graphics *SDL_state = (struct graphics*)instance;
    SDL_DestroyWindow(SDL_state->window);
    SDL_Quit();
    return 0;
}
