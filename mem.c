#include "mem.h"
#include <stdlib.h>
#include <string.h>

/*
https://en.wikibooks.org/wiki/NES_Programming
    CPU memory map:
    (0x0000:0x07ff): internal RAM 2kb
    (0x0800:0x0fff): mirror of (0x000:0x7ff)
    (0x1000:0x17ff): mirror of (0x000:0x7ff)
    (0x1800:0x1fff): mirror of (0x000:0x7ff)
    (0x2000:0x2007): PPU registers
    (0x2000:0x3fff): Mirrors of PPU registers (repeats every 8 bytes)
    (0x4000:0x4017): APU and IO registers
    (0x4018:0x401f): APU and IO test mode registers (normaly disabled)
    (0x4020:0xffff): ROM data
        (0xfffa:0xfffb): NMI vector
        (0xfffc:0xfffd): RST vector
        (0xfffe:0xffff): IRQ/BRK vector

    PPU memory map:
    (0x0000:0x0fff): pattern table 0
    (0x1000:0x1fff): pattern table 1
    (0x2000:0x23ff): nametable 0
    (0x2400:0x27ff): nametable 1
    (0x2800:0x2bff): nametable 2
    (0x2c00:0x2fff): nametable 3
    (0x3000:0x3eff): mirrors of (0x2000:0x2eff)
    (0x3f00:0x3f1f): palette ram indexes
    (0x3f20:0x3fff): mirrors of (0x3f00:0x3f1f)

    PPU internal 0x100 bytes of Object Attribute Memory(OAM):
    (0x00:0x0c)(byte 0): sprite Y coord
    (0x01:0x0d)(byte 1): sprite tile number
    (0x02:0x0e)(byte 2): sprite attributes
    (0x03:0x0f)(byte 3): sprite X coord

    like this???
    struct OAM {
        u8 coord_y;
        u8 tile_nr;
        u8 sprite_attrs;
        u8 coord_x;
    };

*/


extern void write_ppu_reg(u8 reg, u8 val);
extern u8 read_ppu_reg(u8 reg);

struct memory_map {
    u8 *internal_ram;
    u8 *apu_io_regs;
    u8 *ppu_palette_ctrl;
    u8 *ppu_OAM;
    int ppu_OAM_sz;
    /* cart */
    u8 *prg_rom;
    int prg_rom_sz;
    u8 *prg_ram;
    int ptg_ram_sz;
    u8 *chr_rom;
    int chr_rom_sz;
    u8 *ppu_vram;
    int ppu_vram_sz;
} mem;

int mem_init(struct nes_cart *cart)
{
    //MEM_DBG("\n%s\n", __func__);
    mem.internal_ram = (u8*)malloc(0x800);
    memset(mem.internal_ram, 0xff, 0x800);
    mem.apu_io_regs = (u8*)malloc(0x20);
    mem.prg_rom = cart->prg_rom;
    mem.prg_rom_sz = cart->prg_rom_sz * 0x4000;
    mem.chr_rom = cart->chr_rom;
    mem.chr_rom_sz = cart->chr_rom_sz * 0x2000;
    mem.ppu_vram = (u8*)malloc(0x800);
    mem.ppu_vram_sz = 0x800;
    memset(mem.ppu_vram, 0x0,mem.ppu_vram_sz);
    mem.prg_ram = cart->prg_ram;
    mem.ptg_ram_sz = cart->prg_ram_sz * 0x2000;

    mem.ppu_palette_ctrl = (u8*)malloc(0x20);
    mem.ppu_OAM = (u8*)malloc(0x100);
    mem.ppu_OAM_sz = 0x100;
    int i;
    for (i = 0x0; i <= 0x20; i++ ){
        printf("%02x ", mem.chr_rom[i]);
    }
    return 0;
}
extern void idle_cpu(int cycles);

void mem_store(u16 address, u8 value)
{
    MEM_DBG("%s: addr: %04x val: %02x\n", __func__, address, value);
    if (address < 0x2000) {
        mem.internal_ram[address % 0x800] = value;
    } else if (address < 0x4000) {
        write_ppu_reg(address % 0x8, value);
    } else if (address < 0x4020) {
        if (address == 0x4014) {
                printf("OAM DMA: %04x\n", value << 8);
                memcpy(mem.ppu_OAM, mem.internal_ram + ((u16)(value << 8) & 0xff00),  0x100);
                idle_cpu(513 + 1);
                return;
                //getchar();
        }
        mem.apu_io_regs[address % 0x4000] = value;
    } else if (address <= 0xffff){
        /* Mapper 000 */
        if (address >= 0x6000 && address < 0x8000)
            mem.prg_ram[address % 0x6000] = value;
        else if (address >= 0x8000)
            mem.prg_rom[address % mem.prg_rom_sz] = value;
    } else {
        MEM_DBG("%s: OUT OF RANGE addr: %04x = %02x\n", __func__, address, value);
    }
}

u8 mem_load(u16 address)
{
    MEM_DBG("%s: addr: %04x\n", __func__, address);
    if (address < 0x2000) {
        return mem.internal_ram[address % 0x800];
    } else if (address < 0x4000) {
        return read_ppu_reg((address - 0x2000) % 0x8);
    } else if (address < 0x4020) {
        //if (address == 0x4016) { return ~0;}
        if (address == 0x4015) getchar();
        return mem.apu_io_regs[address % 0x4000];
    } else if (address <= 0xffff) {
        /* Mapper 000 */
        if (address >= 0x6000 && address < 0x8000)
            return mem.prg_ram[address % 0x6000];
        else if (address >= 0x8000)
            return mem.prg_rom[address % mem.prg_rom_sz];
    }
    MEM_DBG("%s: OUT OF RANGE addr: %04x\n", __func__, address);
    return 0;
}

u8 discr_mem_load(u16 address)
{
    if (address > 0x2000 && address < 0x4020) return 0;
    return mem_load(address);
}

void mem_store_ppu(u16 address, u8 value)
{
    MEM_DBG("%s: addr: %04x val: %02x\n", __func__, address, value);
    if (address < 0x2000) {
        /* cart can switch banks */
        mem.chr_rom[address] = value;
    } else if (address < 0x3000) {
        /* cart specific */
        if (address >= 0x2800) {
            MEM_DBG("%s: IMPLEMENT VRAM STUFF addr: %04x val: %02x\n", __func__, address, value);
            //return;
        }
        mem.ppu_vram[address % 0x400] = value;
    } else if (address < 0x3f00) {
        mem_store_ppu(address - 0x1000, value);
    } else if (address < 0x4000) {
        if(address < 0x3f0f && address > 0x3f00) {
            MEM_DBG("Palette address %04x write %02x\n", address, value);
        }
        mem.ppu_palette_ctrl[((address & 0xff) % 0x20)] = value;
    } else if (address >= 0x4000) {
        MEM_DBG("%s: OUT OF RANGE addr: %04x = %02x\n", __func__, address, value);
    }
}

u8 mem_load_ppu(u16 address)
{
    MEM_DBG("%s: addr: %04x\n", __func__, address);
    if (address < 0x2000) {
        /* cart can switch banks */
        return mem.chr_rom[address];
    } else if (address < 0x3000) {
        /* cart specific */
        if (address >= 0x2800) {
            MEM_DBG("%s: IMPLEMENT VRAM STUFF address: %04x\n", __func__, address);
            //return 0;
        }
        return mem.ppu_vram[address % 0x800];
    } else if (address < 0x3f00) {
        return mem_load_ppu(address - 0x1000);
    } else if (address < 0x4000) {
        return mem.ppu_palette_ctrl[((address & 0xff) % 0x20)];
    } else if (address >= 0x4000) {
        MEM_DBG("%s: OUT OF RANGE addr: %04x\n", __func__, address);
    }
    return 0;
}

int ppu_get_chrom(u8 **chr)
{
    *chr = mem.chr_rom;
    return mem.chr_rom_sz;
}

int ppu_get_vram(u8 **vram)
{
    *vram = mem.ppu_vram;
    return mem.ppu_vram_sz;
}

int ppu_get_OAM(u8 **oam)
{
    *oam = mem.ppu_OAM;
    return mem.ppu_OAM_sz;
}
