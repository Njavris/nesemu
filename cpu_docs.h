#ifndef __CPU_DOCS_H__
#define __CPU_DOCS_H__

/*
Does not contain code
*/

/*
6502 modes
Indexdexed - uses X and Y registers to determine address
Non-Indexed

Accumulator - operate on A register
IMPLIED - no operand
Immediate - operand is a value
Zero Page - fetches value from an 8-bit address on zero page
Absolute - uses 16-bit address
Relative - uses 8-bit SIGNED offset relative to PC (branch instructions)
Indirect - user 16-bit pointer value

Address Modes:
A     . Accumulator          OPC A        operand is AC
abs   . absolute             OPC $HHLL    operand is address $HHLL
abs,X . absolute, X-indexed  OPC $HHLL,X  operand is address incremented by X with carry
abs,Y . absolute, Y-indexed  OPC $HHLL,Y  operand is address incremented by Y with carry
#     . immediate            OPC #$BB     operand is byte (BB)
impl  . implied              OPC          operand implied
ind   . indirect             OPC ($HHLL)  operand is effective address; effective address is value of address
X,ind . X-indexed, indirect  OPC ($BB,X)  operand is effective zeropage address; effective address is byte (BB) incremented by X without carry
ind,Y . indirect, Y-indexed  OPC ($LL),Y  operand is effective address incremented by Y with carry; effective address is word at zeropage address
rel   . relative             OPC $BB      branch target is PC + offset (BB), bit 7 signifies negative offset
zpg   . zeropage             OPC $LL      operand is of address; address hibyte = zero ($00xx)
zpg,X . zeropage, X-indexed  OPC $LL,X    operand is address incremented by X; address hibyte = zero ($00xx); no page transition
zpg,Y . zeropage, Y-indexed  OPC $LL,Y    operand is address incremented by Y; address hibyte = zero ($00xx); no page transition
*/

/*
Instructions:
ADC - add with carry
AND - bitwise AND with A
ASL - arithmetic shift left
BIT - test bits
BPL - Branch on PLus
BMI - Branch on MInus
BVC - Branch on oVerflow Clear
BVS - Branch on oVerflow Set
BCC - Branch on Carry Clear
BCS - Branch on Carry Set
BNE - Branch on Not Equal
BEQ - Branch on EQual
BRK - causes interrupt increments PC
CMP - compare A
CPX - compare X
CPY - compare Y
DEC - decrement memory
EOR - bitwise XOR
CLC - CLear Carry
SEC - SEt Carry
CLI - CLear Interrupt
SEI - SEt Interrupt
CLV - CLear oVerflow
CLD - CLear Decimal
SED - SEt Decimal
INC - increment memory
JMP - jump
JSR - jump to subroutine
LDA - load A
LDX - load X
LDY - load Y
LSR - logical shift right
NOP - no operation
ORA - bitwise OR with A
TAX - Transfer A to X
TXA - Transfer X to A
DEX - DEcrement X
INX - INcrement X
TAY - Transfer A to Y
TYA - Transfer Y to A
DEY - DEcrement Y
INY - INcrement Y
ROL - rotate left
ROR - rotate right
RTI - return from interrupt
RTS - return from subroutine
SBC - subtract with carry
TXS - Transfer X to Stack ptr
TSX - Transfer Stack ptr to X
PHA - PusH Accumulator
PLA - PuLl Accumulator
PHP - PusH Processor status
PLP - PuLl Processor status
STA - store A
STX - store X
STY - store Y
*/

/*
   Nintendo Entertainment System: CPU memory map

   $0000-$07FF = 2 KB internal RAM
   $0800-$1FFF = Mirrors of $0000-$07FF
   $2000-$2007 = PPU registers
   $2008-$3FFF = Mirrors of $2000-$2007
   $4000-$4017 = APU and I/O registers
   $4018-$FFFF = Cartridge PRG ROM, cartridge PRG RAM, and mapper registers

   $FFFA = NMI vector
   $FFFC = Reset vector
   $FFFE = IRQ/BRK vector

   In some cartridges, $6000-$7FFF is SRAM.

*/

/*
RESOURCES:
https://wiki.nesdev.com/w/index.php/NES_reference_guide
http://e-tradition.net/bytes/6502/6502_instruction_set.html
http://www.6502.org/tutorials/6502opcodes.html#STACK
http://www.emulator101.com/6502-addressing-modes.html
https://github.com/EtchedPixels/Virtual65/blob/master/6502.c
https://code.google.com/archive/p/moarnes/downloads
*/
#endif // __CPU_DOCS_H__
