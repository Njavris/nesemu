#include <stdio.h>
#include <stdlib.h>
#include "ines.h"
#include "common.h"
#include "mem.h"

struct iNes_hdr {
    s8 magic[4];
    u8 prg_rom_sz;  // nr of 16KB chunks
    u8 chr_rom_sz;  // nr of 8KB chunks, 0 means cart uses CHR RAM
    union {
        struct {
            u8 mirroring:1;
            u8 battery:1;
            u8 trainer:1;
            u8 ignore_mirroring:1;
            u8 mapper_nr_l:4;
        };
        u8 flags;
    } flags6;
    union {
        struct {
            u8 unisystem_vs:1;
            u8 play_choice:1;
            u8 is_nes_2:2;
            u8 mapper_nr_h:4;
        };
        u8 flags;
    } flags7;
    u8 prg_ram_sz;  //nr of 8KB chunks, 0 means 8KB - ???
    union {
        struct {
            u8 ntsc_pal:1;
        };
        u8 flags;
    } flags9;
    union {
        struct {
            u8 ntsc_pal:2;
            u8 unused:2;
            u8 prg_ram:1;
            u8 has_bus_conflicts:1;
        };
        u8 flags;
    } flags10;
    u8 zeros[5];
};

int load_file(char *file_name)
{
    struct iNes_hdr hdr;
    struct nes_cart *cart;
    int ret = 0;
    int prg_rom_size = 0x4000;
    int chr_rom_size = 0x2000;
    int prg_ram_size = 0x2000;

    FILE *fd;
    fd = fopen(file_name,"rb");
    if (!fd)
        return -1;

    fread(&hdr,sizeof(struct iNes_hdr),1,fd);

    cart = (struct nes_cart*)malloc(sizeof(struct nes_cart));
    prg_rom_size *= hdr.prg_rom_sz;
    chr_rom_size *= hdr.chr_rom_sz;
    if (!hdr.prg_ram_sz)
        prg_ram_size *= 1;
    else
        prg_ram_size *= hdr.prg_ram_sz;


    cart->prg_rom = (u8*)malloc(prg_rom_size);
    cart->chr_rom = (u8*)malloc(chr_rom_size);
    cart->prg_ram = (u8*)malloc(prg_ram_size);
    cart->prg_rom_sz = hdr.prg_rom_sz;
    cart->chr_rom_sz = hdr.chr_rom_sz;
    cart->prg_ram_sz = hdr.prg_ram_sz;
    cart->mapper_nr = (hdr.flags6.mapper_nr_l) | ((hdr.flags7.mapper_nr_h) << 4);
    cart->mirroring = hdr.flags6.mirroring;

    if (hdr.flags6.trainer)
        fseek(fd, 0x200, SEEK_SET);
    fread((void*)cart->prg_rom, prg_rom_size, 1, fd);
    fread((void*)cart->chr_rom, chr_rom_size, 1, fd);
    fclose(fd);

    ret = mem_init(cart);

    return ret;
}
