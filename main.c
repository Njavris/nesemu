
#include <stdio.h>
#include <stdlib.h>
#include "common.h"
#include "cpu.h"
#include "ppu.h"
#include "mem.h"
#include "ines.h"

#include "window.h"
#include <windows.h>

extern struct cpu_opcode opcode_list[];
FILE *f;

int main(int argc, char** argv)
{

    f = fopen("out.txt", "a");
    int scaling = 2;
    void *window, *ptrn_tbl, *name_tbl;
    u32 *ptrn_buf;//, *name_buf;
    load_file("Donkey Kong (JU) [p1].nes");
    //load_file("super_mario_bros.nes");
    //load_file("nestest.nes");
    //load_file("ntsc_torture.nes");

    ptrn_tbl = spawn_window(256, 128, scaling, "Pattern tables");
    name_tbl = spawn_window(512, 480, 1, "Name tables");
    window = spawn_window(PPU_SCREEN_WIDTH, PPU_SCREEN_HEIGHT, scaling, "NES");
    ptrn_buf = (u32*)malloc(sizeof(u32) * 256 * 128);

    init_ppu();
    init_cpu();
//    check_modes();
    while (1) {
        draw_pattern_tables(ptrn_buf);
        ppu_render();
        put_screen(window, get_framebuffer(), PPU_PIXEL_COUNT);
        put_screen(ptrn_tbl, ptrn_buf, 256 * 128);
        put_screen(name_tbl, get_nametable(), 480 * 512);
    }
    close_window(window);
    close_window(ptrn_tbl);
    close_window(name_tbl);
    return 0;
}
