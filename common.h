#ifndef __COMMON_H__
#define __COMMON_H__

typedef unsigned int u32;
typedef unsigned short u16;
typedef unsigned char u8;

typedef signed int s32;
typedef signed short s16;
typedef signed char s8;

//#define CPU_DEBUG
//#define MEM_DEBUG
//#define PPU_DEBUG
//#define DEBUG_FILE

#endif
