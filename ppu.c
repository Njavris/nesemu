#include "PPU.h"
#include <time.h>
#include "cpu.h"
#include "mem.h"
#include <stdlib.h>

#include "nametable_xmpl.h"

#define NAMETABLE_WIDTH 512
#define NAMETABLE_HEIGHT 480

static struct PPU_state {
    u32 *framebuffer;
    u32 *name_buf;
    u8 *chr_rom;
    int chr_rom_sz;
    u8 *ppu_vram;
    int ppu_vram_sz;
    u8 *ppu_OAM;
    int ppu_OAM_sz;
    /*
        registers
        0 : ppuctrl
        1 : ppumask
        2 : ppustatus
        3 : oamaddr
        4 : oamdata
        5 : ppuscroll
        6 : ppuaddr
        7 : ppudata
        8 : oamdma
    */
    u8 ppu_regs[9];
    u16 ppuaddr_reg;
    u8 addr_latch;
    u8 scroll_latch;
    u8 ppu_scroll_x;
    u8 ppu_scroll_y;
    u8 nmi_en;
    u8 written;
} PPU_state;


struct OAM {
    u8 y_pos; // Y position + 1 of sprite's top
    u8 index; // bit 0 - pattern table, rest select tile (0 - 254);
    u8 attributes;
    u8 x_pos; // X position of sprite's left side
};

#define getVBlank   ((PPU_state.ppu_regs[2] >> 7) & 1)
#define setVBlank   (PPU_state.ppu_regs[2] |= (1 << 7))
#define unsetVBlank (PPU_state.ppu_regs[2] &= ~(1 << 7))

void write_ppu_reg(u8 reg, u8 val)
{
    PPU_state.written = val;
    //return;
    switch (reg) {
    case 0 :
        PPU_state.ppu_regs[0] = val;
        PPU_state.nmi_en = (val >> 7) & 1;
        break;
    case 1 :
        PPU_DBG("%s IMPLEMENT write_ppu_reg 1\n", __func__)
        //getchar();
        break;
    case 3 :
        PPU_state.ppu_regs[3] = val;
        break;
    case 4 :
        PPU_state.ppu_OAM[PPU_state.ppu_regs[3]] = val;
        PPU_state.ppu_regs[3]++;
        break;
    case 5 :
        if (!PPU_state.scroll_latch) {
            PPU_state.ppu_scroll_x = val;
            PPU_state.scroll_latch = 1;
        } else {
            PPU_state.ppu_scroll_y = val;
            PPU_state.scroll_latch = 0;
        }
        break;
    case 6 :
        if (!PPU_state.addr_latch) {
            PPU_state.ppuaddr_reg &= 0xff;
            PPU_state.ppuaddr_reg |= (val << 8);
            PPU_state.addr_latch = 1;
        } else {
            PPU_state.ppuaddr_reg &= 0xff << 8;
            PPU_state.ppuaddr_reg |= val & 0xff;
            PPU_state.addr_latch = 0;
        }
        //if (PPU_state.ppuaddr_reg >= 0x3f00) {printf("Write 2006: %02x ppu addr: %04x, %02x+++++++++++++++++++++++++++++++++\n", val, PPU_state.ppuaddr_reg, val); getchar();}
        break;
    case 7 :
        //mem_store_ppu(PPU_state.ppuaddr_reg, val);
        PPU_state.ppu_vram[PPU_state.ppuaddr_reg % 0x800] = val;
        //if (PPU_state.ppuaddr_reg >= 0x3f00) {printf("Write 2007: %02x ppu addr: %04x, %02x########################################\n", val, PPU_state.ppuaddr_reg, val); getchar();}
        //if (MOS6502_state.A != 0x24) getchar();
        PPU_state.ppuaddr_reg += (PPU_state.ppu_regs[0] & (1 << 2)) ? 32 : 1;
        PPU_state.ppuaddr_reg &= 0x3FFF;
        break;
    }
}

u8 read_ppu_reg(u8 reg)
{
    //return PPU_state.ppu_regs[reg];
    u8 ret;
    switch (reg) {
    case 2 :
        ret = (getVBlank << 7) | (PPU_state.written & 0x1f);
        unsetVBlank;
        PPU_state.addr_latch = 0;
        PPU_state.scroll_latch = 0;
        break;
    case 4 :
        ret = PPU_state.ppu_OAM[PPU_state.ppu_regs[3]];
        break;
    case 7 :
        getchar();
        PPU_state.ppu_regs[reg] = mem_load_ppu(PPU_state.ppuaddr_reg);
        PPU_state.ppuaddr_reg += (PPU_state.ppu_regs[0] & (1 << 2)) ? 32 : 1;
        ret = PPU_state.ppu_regs[reg];
        PPU_state.ppuaddr_reg &= 0x3FFF;
        break;
    }
    return ret;
}

u32 *get_rand_framebuffer(void)
{
    int i;
    for (i = 0; i < PPU_PIXEL_COUNT; i++) {
        int intensity = (rand() & 0xff);
        unsigned whitenoise = (intensity << 0x10) | (intensity << 0x8) | intensity;
        PPU_state.framebuffer[i] = whitenoise;
    }
    return PPU_state.framebuffer;
}

int init_ppu(void)
{
    srand(time(NULL));
    PPU_state.framebuffer
        = (u32*)malloc((PPU_PIXEL_COUNT + 21 * PPU_SCREEN_WIDTH) * sizeof(u32));
    PPU_state.name_buf = (u32*)malloc(sizeof(u32) * NAMETABLE_WIDTH * NAMETABLE_HEIGHT);
    PPU_state.ppu_vram_sz = ppu_get_vram(&PPU_state.ppu_vram);
    PPU_state.chr_rom_sz = ppu_get_chrom(&PPU_state.chr_rom);
    PPU_state.ppu_OAM_sz = ppu_get_OAM(&PPU_state.ppu_OAM);

    PPU_state.ppu_regs[2] = 0x10 | (1 << 7);
    setVBlank;
    PPU_state.nmi_en = 1;

    //memcpy(PPU_state.ppu_vram, nametable_example, 0x800);
    return 0;
}

static void copy_pattern(u32 *dst, u8 pattern_nr, u8 table_nr, int pos_x, int pos_y, int width)
{
    int x, y;
    int offset = table_nr * 0x1000 + pattern_nr * 0x10;
    for (y = 0 ; y < 8; y++){
        for (x = 0; x < 8; x++) {
            int bit_0, bit_1, pixel, value;
            bit_0 = (PPU_state.chr_rom[offset + y] >> (7 - x)) & 1;
            bit_1 = (PPU_state.chr_rom[offset + y + 8] >> (7 - x )) & 1;
            pixel = bit_0 | (bit_1 << 1);
            if (pixel == 0)
                value = 0;
            else if (pixel == 1)
                value = 0xff;
            else if (pixel == 2)
                value = 0xff << 8;
            else if (pixel == 3)
                value = 0xff << 16;
            dst[(pos_y + y) * width + pos_x + x] = value;
        }
    }
}

static void ppu_dump_pattern_table(int table_nr, u32 *dst, int x_offs, int y_offs) {
    int x, y;
    for (y = 0; y < 0x10; y++) {
        for (x = 0; x < 0x10; x++) {
            copy_pattern(dst, y * 0x10 + x, table_nr, x * 8 + x_offs, y * 8 + y_offs, PPU_SCREEN_WIDTH);
        }
    }
}

void draw_pattern_tables(u32 *dst)
{
    ppu_dump_pattern_table(0, dst, 0, 0);
    ppu_dump_pattern_table(1, dst, 128, 0);
    //ppu_dump_pattern_table(3, dst, 0, 128);
    //ppu_dump_pattern_table(4, dst, 128, 128);
}

static void ppu_draw_name_table(int table_nr, u32 *dst, int x_offs, int y_offs) {
    int x, y;
    for (y = 0; y < 30; y++) {
        for (x = 0; x < 32; x++) {
            u8 tile_id = PPU_state.ppu_vram[(table_nr * 0x400) + (y * 32) + x];
            //tile_id = 0x1;
            copy_pattern(dst, tile_id,(PPU_state.ppu_regs[0] >> 4) & 1, x * 8 + x_offs, y * 8 + y_offs, 512);
        }
    }
}

u32 *get_framebuffer(void)
{
    int i, x, y;
    struct OAM *tbl;
    /* draw background */
    for (y = 0; y < PPU_SCREEN_HEIGHT; y++) {
        for(x = 0; x < PPU_SCREEN_WIDTH; x++) {
            PPU_state.framebuffer[y * PPU_SCREEN_WIDTH + x]
                = PPU_state.name_buf[((y + PPU_state.ppu_scroll_y) * NAMETABLE_WIDTH) + x + PPU_state.ppu_scroll_x];
        }
    }

    /* draw sprites */
    tbl = (struct OAM*)PPU_state.ppu_OAM;
    for (i = 0; i < 64; i++) {
        printf ("sprite %02x X:%02x Y:%02x tile:%02x %08x\n", i, tbl->x_pos, tbl->y_pos, tbl->index & 0xfe, tbl);
        copy_pattern(PPU_state.framebuffer, tbl->index & 0xfe, tbl->index & 1, tbl->x_pos, tbl->y_pos, PPU_SCREEN_WIDTH);
        tbl++;
    }

    draw_name_tables();
    return PPU_state.framebuffer;
}

u32 *get_nametable(void)
{
    return PPU_state.name_buf;
}

void draw_name_tables(void)
{
    ppu_draw_name_table(0, PPU_state.name_buf, 0, 0);
    ppu_draw_name_table(1, PPU_state.name_buf, 256, 0);
    ppu_draw_name_table(2, PPU_state.name_buf, 0, 240);
    ppu_draw_name_table(3, PPU_state.name_buf, 256, 240);
}

int ppu_render(void)
{
    int i, j;
    /* Zero scan line */
    //printf("scanline 0\n");
    //getchar();
    for (j = 0; j < 114; j++) cpu_cycle();
    /* Rest */
    j = 0;
    for (i = 1; i < 262; i++) {
        if (i % 3) j--;
            //printf("Scanline %d\n", i);
        if (i == 240) {
            /* VBlank */
            setVBlank;
            if (nmi_set()) non_maskable_interrupt();
        }
        if (i > 240) {
            /* VBlank */
            j += 114;
        } else {
            /* Drawing */
            j += 28;
            j += 86;
        }

        for ( ; j > 0; j--) {
            cpu_cycle();
        }
    }
    unsetVBlank;
    return 0;
}

int nmi_set(void)
{
    return PPU_state.nmi_en && ((PPU_state.ppu_regs[0] >> 7) & 1);
}

void set_vblank(u8 val)
{
    PPU_state.ppu_regs[0] &= ~(1 <<7);
    PPU_state.ppu_regs[0] |= (val << 7);
}

//http://www.dustmop.io/blog/2015/04/28/nes-graphics-part-1/
//http://www.dustmop.io/blog/2015/06/08/nes-graphics-part-2/
//http://www.dustmop.io/blog/2015/12/18/nes-graphics-part-3/
