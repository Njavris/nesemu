#include "cpu.h"

extern void mem_store(u16 address, u8 value);
extern u8 mem_load(u16 address);
extern u8 discr_mem_load(u16 address);

/* Stack functions */
void stack_push8(u8 value)
{
    mem_store(STACK_BASE + MOS6502_state.SP, value);
    MOS6502_state.SP--;
}
void stack_push16(u16 value)
{
    mem_store(STACK_BASE + MOS6502_state.SP, (u8)(value >> 8));
    mem_store(STACK_BASE + ((u8)(MOS6502_state.SP - 1) & 0xff), (u8)(value & 0xff));
    MOS6502_state.SP -= 2;
}
u8 stack_pull8(void)
{
    u8 ret;
    MOS6502_state.SP++;
    ret = mem_load(STACK_BASE + MOS6502_state.SP);
    return ret;
}
u16 stack_pull16(void)
{
    u16 ret;
    ret = mem_load(STACK_BASE + MOS6502_state.SP + 1) & 0xff;
    ret |= mem_load(STACK_BASE + MOS6502_state.SP + 2) << 8;
    MOS6502_state.SP += 2;
    return ret;
}

/* addressing functions */
void absol(void)
{
    MOS6502_state.curr_operand_address = MOS6502_state.curr_operand.address;
    MOS6502_state.curr_operand_value = discr_mem_load(MOS6502_state.curr_operand_address);
}
void absol_x(void)
{
    u8 page_nr = (MOS6502_state.curr_operand.address >> 8) & 0xff;
    MOS6502_state.curr_operand_address = MOS6502_state.curr_operand.address;
    MOS6502_state.curr_operand_address += MOS6502_state.X;
    MOS6502_state.curr_operand_value = discr_mem_load(MOS6502_state.curr_operand_address);
    if (page_nr == ((MOS6502_state.curr_operand_value >> 8) & 0xff)) MOS6502_state.cycles_inactive++;
}
void absol_y(void)
{
    u8 page_nr = (MOS6502_state.curr_operand.address >> 8) & 0xff;
    MOS6502_state.curr_operand_address = MOS6502_state.curr_operand.address;
    MOS6502_state.curr_operand_address += MOS6502_state.X;
    MOS6502_state.curr_operand_value = discr_mem_load(MOS6502_state.curr_operand_address);
    if (page_nr == ((MOS6502_state.curr_operand_value >> 8) & 0xff)) MOS6502_state.cycles_inactive++;
}
void acc(void) { MOS6502_state.curr_operand_value = MOS6502_state.A; }
void impl(void) {}
void imm(void)
{
    MOS6502_state.curr_operand_address = MOS6502_state.PC + 1/*curr_operand.par[0]*/;
    MOS6502_state.curr_operand_value = mem_load(MOS6502_state.PC + 1);
}
void ind(void)
{
    MOS6502_state.curr_operand_address = discr_mem_load(MOS6502_state.curr_operand.address);
    MOS6502_state.curr_operand_address |= discr_mem_load(MOS6502_state.curr_operand.address + 1) << 8;
    MOS6502_state.curr_operand_value = discr_mem_load(MOS6502_state.curr_operand_address);
}
void ind_x(void)
{
    u16 addr = MOS6502_state.curr_operand.address + MOS6502_state.X;
    MOS6502_state.curr_operand_address = discr_mem_load(addr);
    MOS6502_state.curr_operand_address |= discr_mem_load(addr + 1) << 8;
    MOS6502_state.curr_operand_value = discr_mem_load(MOS6502_state.curr_operand_address);
}
void ind_y(void)
{
    u8 page_nr, addr = MOS6502_state.curr_operand.address;
    MOS6502_state.curr_operand_address = discr_mem_load(addr);
    MOS6502_state.curr_operand_address |= discr_mem_load(addr + 1) << 8;
    page_nr = (MOS6502_state.curr_operand_address >> 8) & 0xff;
    //printf("INDY b4: %04x\n", MOS6502_state.curr_operand_address);
    MOS6502_state.curr_operand_address += MOS6502_state.Y;
    MOS6502_state.curr_operand_value = discr_mem_load(MOS6502_state.curr_operand_address);
    if (page_nr == ((MOS6502_state.curr_operand_value >> 8) & 0xff)) MOS6502_state.cycles_inactive++;
}
void rel(void)
{
    MOS6502_state.curr_operand_address = MOS6502_state.PC + (s8)MOS6502_state.curr_operand.par[0] + 2;
}
void zp(void)
{
    MOS6502_state.curr_operand_address = (MOS6502_state.curr_operand.par[0]) & 0xff;
    MOS6502_state.curr_operand_value = discr_mem_load(MOS6502_state.curr_operand_address);
}
void zp_x(void)
{
    MOS6502_state.curr_operand_address = (MOS6502_state.curr_operand.par[0] + MOS6502_state.X) & 0xff;
    MOS6502_state.curr_operand_value = discr_mem_load(MOS6502_state.curr_operand_address);
}
void zp_y(void)
{
    MOS6502_state.curr_operand_address = (MOS6502_state.curr_operand.par[0] + MOS6502_state.Y) & 0xff;
    MOS6502_state.curr_operand_value = discr_mem_load(MOS6502_state.curr_operand_address);
}

static void (*addr_funcs[])(void) = {
    [ABSOLUTE] = &absol,
    [ABSOLUTE_X] = &absol_x,
    [ABSOLUTE_Y] = &absol_y,
    [ACCUMULATOR] = &acc,
    [IMMEDIATE] = &imm,
    [IMPLIED] = &impl,
    [INDIRECT] = &ind,
    [INDIRECT_X] = &ind_x,
    [INDIRECT_Y] = &ind_y,
    [RELATIVE] = &rel,
    [ZERO_PAGE] = &zp,
    [ZERO_PAGE_X] = &zp_x,
    [ZERO_PAGE_Y] = &zp_y,
};

char* addr_modes[] = {
    [ABSOLUTE] = "ABSOLUTE",
    [ABSOLUTE_X] = "ABSOLUTE_X",
    [ABSOLUTE_Y] = "ABSOLUTE_Y",
    [ACCUMULATOR] = "ACCUMULATOR",
    [IMMEDIATE] = "IMMEDIATE",
    [IMPLIED] = "IMPLIED",
    [INDIRECT] = "INDIRECT",
    [INDIRECT_X] = "INDIRECT_X",
    [INDIRECT_Y] = "INDIRECT_Y",
    [RELATIVE] = "RELATIVE",
    [ZERO_PAGE] = "ZERO_PAGE",
    [ZERO_PAGE_X] = "ZERO_PAGE_X",
    [ZERO_PAGE_Y] = "ZERO_PAGE_Y",
};

/* Utility functions */
static inline void updateC(u16 reg){ MOS6502_state.SR.C = (reg & 0xff00) ? 1 : 0; }
static inline void updateN(u8 reg){ MOS6502_state.SR.N = (reg & 0x80) ? 1 : 0; }
static inline void updateZ(u8 reg){ MOS6502_state.SR.Z = (reg & 0xff) ? 0 : 1; }
static inline void updateV(u8 reg, u8 val, u16 res)
{
    u16 tmp = (res ^ reg) & (res ^ val) & 0x80;
    MOS6502_state.SR.V = tmp ? 1 : 0;
}
static inline void update_flags(int fC, int fN, int fZ, u16 val)
{
    if (fC) updateC(val);
    if (fN) updateN((u8)val);
    if (fZ) updateZ((u8)val);
}
void branch(int taken)
{
    if (taken) {
        CPU_DBG("%s: Branch taken!\n", __func__);
        u16 page_nr = MOS6502_state.curr_operand_address & 0xff00;
        u16 new_page_nr = MOS6502_state.PC & 0xff00;
        MOS6502_state.cycles_inactive += (new_page_nr == page_nr) ? 1 : 2;
        MOS6502_state.PC = MOS6502_state.curr_operand_address - MOS6502_state.curr_op_length;
    }
}

void value_store(void)
{
    if (MOS6502_state.curr_opcode_mode == ACCUMULATOR)
        MOS6502_state.A = MOS6502_state.curr_operand_value;
    else
        mem_store(MOS6502_state.curr_operand_address, MOS6502_state.curr_operand_value);
}


/* Instruction definitions */

void dec_fn(void)
{
    MOS6502_state.curr_operand_value--;
    //MOS6502_state.curr_operand_value &= 0xff;
    update_flags(0,1,1, MOS6502_state.curr_operand_value);
    value_store();
}
void dex_fn(void)
{
    MOS6502_state.X--;
    //MOS6502_state.X &= 0xff;
    update_flags(0,1,1, MOS6502_state.X);
}
void dey_fn(void)
{
    MOS6502_state.Y--;
    //MOS6502_state.Y &= 0xff;
    update_flags(0,1,1, MOS6502_state.Y);
}
void inc_fn(void)
{
    MOS6502_state.curr_operand_value++;
    //MOS6502_state.curr_operand_value &= 0xff;
    update_flags(0,1,1, MOS6502_state.curr_operand_value);
    value_store();
}
void inx_fn(void)
{
    MOS6502_state.X++;
    MOS6502_state.X &= 0xff;
    update_flags(0,1,1, MOS6502_state.X);
}
void iny_fn(void)
{
    MOS6502_state.Y++;
    MOS6502_state.Y &= 0xff;
    update_flags(0,1,1, MOS6502_state.Y);
}
void rol_fn(void)
{
    MOS6502_state.curr_operand_value <<= 1;
    MOS6502_state.curr_operand_value |= MOS6502_state.SR.C;
    update_flags(1,1,1,MOS6502_state.curr_operand_value);
    value_store();
}
void ror_fn(void)
{
    MOS6502_state.SR.C = MOS6502_state.curr_operand_value & 1;
    MOS6502_state.curr_operand_value >>= 1;
    MOS6502_state.curr_operand_value |= MOS6502_state.SR.C << 7;
    update_flags(0,1,1,MOS6502_state.curr_operand_value);
    value_store();
}
void sbc_fn(void)
{
    u16 res = MOS6502_state.A + (s8)MOS6502_state.curr_operand_value + MOS6502_state.SR.C;
    update_flags(1,1,1,res);
    updateV(MOS6502_state.A, MOS6502_state.curr_operand_value, res);
}
void ora_fn(void)
{
    MOS6502_state.A |= MOS6502_state.curr_operand_value;
    update_flags(0,1,1,MOS6502_state.A);
}
void eor_fn(void)
{
    MOS6502_state.A ^= MOS6502_state.curr_operand_value;
    update_flags(0,1,1,MOS6502_state.A);
}
void adc_fn(void)
{
    u16 res = MOS6502_state.A + MOS6502_state.curr_operand_value + MOS6502_state.SR.C;
    update_flags(1,1,1,res);
    updateV(MOS6502_state.A, MOS6502_state.curr_operand_value, res);
    MOS6502_state.A = (u8)res;
}
void and_fn(void)
{
    MOS6502_state.A &= MOS6502_state.curr_operand_value;
    update_flags(0,1,1,MOS6502_state.A);
}
void asl_fn(void)
{
    MOS6502_state.curr_operand_value <<= 1;
    update_flags(1,1,1,MOS6502_state.curr_operand_value);
    value_store();
}
void bit_fn(void)
{
    u16 res = MOS6502_state.curr_operand_value & MOS6502_state.A;
    updateZ(res);
    MOS6502_state.SR.flags &= ~(0x3 << 6);
    MOS6502_state.SR.flags |= MOS6502_state.curr_operand_value & (0x3 << 6);
}
void cmp_fn(void)
{
    u16 res = MOS6502_state.A - (MOS6502_state.curr_operand_value & 0xff);
    update_flags(0,1,0, res);
    MOS6502_state.SR.C = (MOS6502_state.A >= (MOS6502_state.curr_operand_value & 0xff)) ? 1 : 0;
    MOS6502_state.SR.Z = (MOS6502_state.A == (MOS6502_state.curr_operand_value & 0xff)) ? 1 : 0;
}
void cpx_fn(void)
{
    u16 res = MOS6502_state.X - (MOS6502_state.curr_operand_value & 0xff);
    update_flags(0,1,0, res);
    MOS6502_state.SR.C = (MOS6502_state.X >= (MOS6502_state.curr_operand_value & 0xff)) ? 1 : 0;
    MOS6502_state.SR.Z = (MOS6502_state.X == (MOS6502_state.curr_operand_value & 0xff)) ? 1 : 0;
}
void cpy_fn(void)
{
    u16 res = MOS6502_state.Y - (MOS6502_state.curr_operand_value & 0xff);
    update_flags(0,1,0, res);
    MOS6502_state.SR.C = (MOS6502_state.Y >= (MOS6502_state.curr_operand_value & 0xff)) ? 1 : 0;
    MOS6502_state.SR.Z = (MOS6502_state.Y == (MOS6502_state.curr_operand_value & 0xff)) ? 1 : 0;
}
void brk_fn(void)
{
    stack_push8(MOS6502_state.SR.flags | (1 << 4));
    stack_push16(MOS6502_state.PC + 2);
    MOS6502_state.SR.I = 1;
    MOS6502_state.PC = (mem_load(IRQ_VEC_1) | (mem_load(IRQ_VEC_0) << 8)) - 1;
}
void bcc_fn(void){ branch(!MOS6502_state.SR.C); }
void bcs_fn(void){ branch(MOS6502_state.SR.C); }
void beq_fn(void){ branch(MOS6502_state.SR.Z); }
void bne_fn(void){ branch(!MOS6502_state.SR.Z); }
void bmi_fn(void){ branch(MOS6502_state.SR.N); }
void bpl_fn(void){ branch(!MOS6502_state.SR.N); }
void bvc_fn(void){ branch(!MOS6502_state.SR.V); }
void bvs_fn(void){ branch(MOS6502_state.SR.V); }
void rti_fn(void){ MOS6502_state.SR.flags = stack_pull8(); MOS6502_state.PC = stack_pull16() - 1;}
void rts_fn(void){ MOS6502_state.PC = stack_pull16();}
void jmp_fn(void){ MOS6502_state.PC = MOS6502_state.curr_operand_address - 3;}
void jsr_fn(void){ stack_push16(MOS6502_state.PC + 2); MOS6502_state.PC = MOS6502_state.curr_operand_address - 3;}
void clc_fn(void){ MOS6502_state.SR.C = 0; }
void cld_fn(void){ MOS6502_state.SR.D = 0; }
void cli_fn(void){ MOS6502_state.SR.I = 0; }
void clv_fn(void){ MOS6502_state.SR.V = 0; }
void lda_fn(void)
{
    MOS6502_state.A = mem_load(MOS6502_state.curr_operand_address);
    update_flags(0, 1, 1, MOS6502_state.A);
}
void ldx_fn(void)
{
    MOS6502_state.X = mem_load(MOS6502_state.curr_operand_address);
    update_flags(0, 1, 1, MOS6502_state.X);
}
void ldy_fn(void)
{
    MOS6502_state.Y = mem_load(MOS6502_state.curr_operand_address);
    update_flags(0, 1, 1, MOS6502_state.Y);
}
void lsr_fn(void)
{
    if (MOS6502_state.curr_operand_value & 1) MOS6502_state.SR.C = 1;
    else MOS6502_state.SR.C = 0;
    MOS6502_state.curr_operand_value >>= 1;
    update_flags(0, 1, 1, MOS6502_state.SR.flags);
    value_store();
}
void nop_fn(void){ /*nuffin*/ }
void pha_fn(void){ stack_push8(MOS6502_state.A); }
void php_fn(void){ stack_push8(MOS6502_state.SR.flags | (1 << 4)); }
void pla_fn(void){ MOS6502_state.A = stack_pull8(); update_flags(0, 1, 1, MOS6502_state.A);}
void plp_fn(void){ MOS6502_state.SR.flags = stack_pull8() | (1 << 5); }
void sec_fn(void){ MOS6502_state.SR.C = 1; }
void sed_fn(void){ MOS6502_state.SR.D = 1; }
void sei_fn(void){ MOS6502_state.SR.I = 1; }
void sta_fn(void){ mem_store(MOS6502_state.curr_operand_address, MOS6502_state.A); }
void stx_fn(void){ mem_store(MOS6502_state.curr_operand_address, MOS6502_state.X); }
void sty_fn(void){ mem_store(MOS6502_state.curr_operand_address, MOS6502_state.Y); }
void tax_fn(void)
{
    MOS6502_state.X = MOS6502_state.A;
    update_flags(0, 1, 1, MOS6502_state.X);
}
void tay_fn(void)
{
    MOS6502_state.Y = MOS6502_state.A;
    update_flags(0, 1, 1, MOS6502_state.Y);
}
void tsx_fn(void)
{
    MOS6502_state.X = MOS6502_state.SP;
    update_flags(0, 1, 1, MOS6502_state.X);
}
void txa_fn(void)
{
    MOS6502_state.A = MOS6502_state.X;
    update_flags(0, 1, 1, MOS6502_state.A);
}
void txs_fn(void)
{
    MOS6502_state.SP = MOS6502_state.X;
    //update_flags(0, 1, 1, MOS6502_state.SP);
}
void tya_fn(void)
{
    MOS6502_state.A = MOS6502_state.Y;
    update_flags(0, 1, 1, MOS6502_state.A);
}

#define OPCODE(A, B, D, E, F, G)\
[A] = {                  \
    .opcode = A,         \
    .name = #B,          \
    .cycles = D,         \
    .access_cycle = E,   \
    .mode = F,           \
    .op_func = G,    \
},

struct cpu_opcode opcode_list[0xff] = {
    OPCODE(0x69, adc, 2, 0, IMMEDIATE,   &adc_fn)
    OPCODE(0x65, adc, 3, 0, ZERO_PAGE,   &adc_fn)
    OPCODE(0x75, adc, 4, 0, ZERO_PAGE_X, &adc_fn)
    OPCODE(0x61, adc, 6, 0, INDIRECT_X,  &adc_fn)
    OPCODE(0x71, adc, 5, 1, INDIRECT_Y,  &adc_fn)
    OPCODE(0x6d, adc, 4, 0, ABSOLUTE,    &adc_fn)
    OPCODE(0x7d, adc, 4, 1, ABSOLUTE_X,  &adc_fn)
    OPCODE(0x79, adc, 4, 1, ABSOLUTE_Y,  &adc_fn)
    OPCODE(0x29, and, 2, 0, IMMEDIATE,   &and_fn)
    OPCODE(0x25, and, 3, 0, ZERO_PAGE,   &and_fn)
    OPCODE(0x35, and, 4, 0, ZERO_PAGE_X, &and_fn)
    OPCODE(0x21, and, 6, 0, INDIRECT_X,  &and_fn)
    OPCODE(0x31, and, 5, 1, INDIRECT_Y,  &and_fn)
    OPCODE(0x2d, and, 4, 0, ABSOLUTE,    &and_fn)
    OPCODE(0x3d, and, 4, 1, ABSOLUTE_X,  &and_fn)
    OPCODE(0x39, and, 4, 1, ABSOLUTE_Y,  &and_fn)
    OPCODE(0x0a, asl, 2, 0, ACCUMULATOR, &asl_fn)
    OPCODE(0x06, asl, 5, 0, ZERO_PAGE,   &asl_fn)
    OPCODE(0x16, asl, 6, 0, ZERO_PAGE_X, &asl_fn)
    OPCODE(0x0e, asl, 6, 0, ABSOLUTE,    &asl_fn)
    OPCODE(0x1e, asl, 7, 0, ABSOLUTE_X,  &asl_fn)
    OPCODE(0x90, bcc, 2, 2, RELATIVE,    &bcc_fn)
    OPCODE(0xb0, bcs, 2, 2, RELATIVE,    &bcs_fn)
    OPCODE(0xf0, beq, 2, 2, RELATIVE,    &beq_fn)
    OPCODE(0x30, bmi, 2, 2, RELATIVE,    &bmi_fn)
    OPCODE(0xd0, bne, 2, 2, RELATIVE,    &bne_fn)
    OPCODE(0x10, bpl, 2, 2, RELATIVE,    &bpl_fn)
    OPCODE(0x50, bvc, 2, 2, RELATIVE,    &bvc_fn)
    OPCODE(0x70, bvs, 2, 2, RELATIVE,    &bvs_fn)
    OPCODE(0x24, bit, 3, 0, ZERO_PAGE,   &bit_fn)
    OPCODE(0x2c, bit, 4, 0, ABSOLUTE,    &bit_fn)
    OPCODE(0x00, brk, 7, 0, IMPLIED,     &brk_fn)
    OPCODE(0x18, clc, 2, 0, IMPLIED,     &clc_fn)
    OPCODE(0xd8, cld, 2, 0, IMPLIED,     &cld_fn)
    OPCODE(0x58, cli, 2, 0, IMPLIED,     &cli_fn)
    OPCODE(0xb8, clv, 2, 0, IMPLIED,     &clv_fn)
    OPCODE(0xc9, cmp, 2, 0, IMMEDIATE,   &cmp_fn)
    OPCODE(0xc5, cmp, 3, 0, ZERO_PAGE,   &cmp_fn)
    OPCODE(0xd5, cmp, 4, 0, ZERO_PAGE_X, &cmp_fn)
    OPCODE(0xd1, cmp, 5, 0, INDIRECT_Y,  &cmp_fn)
    OPCODE(0xc1, cmp, 6, 1, INDIRECT_X,  &cmp_fn)
    OPCODE(0xcd, cmp, 4, 0, ABSOLUTE,    &cmp_fn)
    OPCODE(0xdd, cmp, 4, 1, ABSOLUTE_X,  &cmp_fn)
    OPCODE(0xd9, cmp, 4, 1, ABSOLUTE_Y,  &cmp_fn)
    OPCODE(0xe0, cpx, 2, 0, IMMEDIATE,   &cpx_fn)
    OPCODE(0xe4, cpx, 3, 0, ZERO_PAGE,   &cpx_fn)
    OPCODE(0xec, cpx, 4, 0, ABSOLUTE,    &cpx_fn)
    OPCODE(0xc0, cpy, 2, 0, IMMEDIATE,   &cpy_fn)
    OPCODE(0xc4, cpy, 3, 0, ZERO_PAGE,   &cpy_fn)
    OPCODE(0xcc, cpy, 4, 0, ABSOLUTE,    &cpy_fn)
    OPCODE(0xc6, dec, 5, 0, ZERO_PAGE,   &dec_fn)
    OPCODE(0xd6, dec, 6, 0, ZERO_PAGE_X, &dec_fn)
    OPCODE(0xce, dec, 3, 0, ABSOLUTE,    &dec_fn)
    OPCODE(0xde, dec, 7, 0, ABSOLUTE_X,  &dec_fn)
    OPCODE(0xca, dex, 2, 0, IMPLIED,     &dex_fn)
    OPCODE(0x88, dey, 2, 0, IMPLIED,     &dey_fn)
    OPCODE(0x49, eor, 2, 0, IMMEDIATE,   &eor_fn)
    OPCODE(0x45, eor, 3, 0, ZERO_PAGE,   &eor_fn)
    OPCODE(0x55, eor, 4, 0, ZERO_PAGE_X, &eor_fn)
    OPCODE(0x41, eor, 6, 0, INDIRECT_X,  &eor_fn)
    OPCODE(0x51, eor, 5, 1, INDIRECT_Y,  &eor_fn)
    OPCODE(0x4d, eor, 4, 0, ABSOLUTE,    &eor_fn)
    OPCODE(0x5d, eor, 4, 1, ABSOLUTE_X,  &eor_fn)
    OPCODE(0x59, eor, 4, 1, ABSOLUTE_Y,  &eor_fn)
    OPCODE(0xe6, inc, 5, 0, ZERO_PAGE,   &inc_fn)
    OPCODE(0xf6, inc, 6, 0, ZERO_PAGE_X, &inc_fn)
    OPCODE(0xee, inc, 6, 0, ABSOLUTE,    &inc_fn)
    OPCODE(0xfe, inc, 7, 0, ABSOLUTE_X,  &inc_fn)
    OPCODE(0xe8, inx, 2, 0, IMPLIED,     &inx_fn)
    OPCODE(0xc8, iny, 2, 0, IMPLIED,     &iny_fn)
    OPCODE(0x4c, jmp, 3, 0, ABSOLUTE,    &jmp_fn)
    OPCODE(0x6c, jmp, 5, 0, INDIRECT,    &jmp_fn)
    OPCODE(0x20, jsr, 6, 0, ABSOLUTE,    &jsr_fn)
    OPCODE(0xa9, lda, 2, 0, IMMEDIATE,   &lda_fn)
    OPCODE(0xa5, lda, 3, 0, ZERO_PAGE,   &lda_fn)
    OPCODE(0xb5, lda, 4, 0, ZERO_PAGE_X, &lda_fn)
    OPCODE(0xa1, lda, 6, 0, INDIRECT_X,  &lda_fn)
    OPCODE(0xb1, lda, 5, 1, INDIRECT_Y,  &lda_fn)
    OPCODE(0xad, lda, 4, 0, ABSOLUTE,    &lda_fn)
    OPCODE(0xbd, lda, 4, 1, ABSOLUTE_X,  &lda_fn)
    OPCODE(0xb9, lda, 4, 1, ABSOLUTE_Y,  &lda_fn)
    OPCODE(0xa2, ldx, 2, 0, IMMEDIATE,   &ldx_fn)
    OPCODE(0xa6, ldx, 3, 0, ZERO_PAGE,   &ldx_fn)
    OPCODE(0xb6, ldx, 4, 0, ZERO_PAGE_Y, &ldx_fn)
    OPCODE(0xae, ldx, 4, 0, ABSOLUTE,    &ldx_fn)
    OPCODE(0xbe, ldx, 4, 1, ABSOLUTE_Y,  &ldx_fn)
    OPCODE(0xa0, ldy, 2, 0, IMMEDIATE,   &ldy_fn)
    OPCODE(0xa4, ldy, 3, 0, ZERO_PAGE,   &ldy_fn)
    OPCODE(0xb4, ldy, 4, 0, ZERO_PAGE_X, &ldy_fn)
    OPCODE(0xac, ldy, 4, 0, ABSOLUTE,    &ldy_fn)
    OPCODE(0xbc, ldy, 4, 1, ABSOLUTE_X,  &ldy_fn)
    OPCODE(0x4a, lsr, 2, 0, ACCUMULATOR, &lsr_fn)
    OPCODE(0x46, lsr, 5, 0, ZERO_PAGE,   &lsr_fn)
    OPCODE(0x56, lsr, 6, 0, ZERO_PAGE_X, &lsr_fn)
    OPCODE(0x4e, lsr, 6, 0, ABSOLUTE,    &lsr_fn)
    OPCODE(0x5e, lsr, 7, 0, ABSOLUTE_X,  &lsr_fn)
    OPCODE(0xea, nop, 2, 0, IMPLIED,     &nop_fn)
    OPCODE(0x01, ora, 5, 1, INDIRECT_X,  &ora_fn)
    OPCODE(0x11, ora, 6, 0, INDIRECT_Y,  &ora_fn)
    OPCODE(0x05, ora, 3, 0, ZERO_PAGE,   &ora_fn)
    OPCODE(0x15, ora, 4, 0, ZERO_PAGE_X, &ora_fn)
    OPCODE(0x09, ora, 2, 0, IMMEDIATE,   &ora_fn)
    OPCODE(0x0d, ora, 4, 0, ABSOLUTE,    &ora_fn)
    OPCODE(0x19, ora, 4, 1, ABSOLUTE_Y,  &ora_fn)
    OPCODE(0x1d, ora, 4, 1, ABSOLUTE_X,  &ora_fn)
    OPCODE(0x48, pha, 3, 0, IMPLIED,     &pha_fn)
    OPCODE(0x08, php, 3, 0, IMPLIED,     &php_fn)
    OPCODE(0x68, pla, 4, 0, IMPLIED,     &pla_fn)
    OPCODE(0x28, plp, 4, 0, IMPLIED,     &plp_fn)
    OPCODE(0x2a, rol, 2, 0, ACCUMULATOR, &rol_fn)
    OPCODE(0x26, rol, 5, 0, ZERO_PAGE,   &rol_fn)
    OPCODE(0x36, rol, 6, 0, ZERO_PAGE_X, &rol_fn)
    OPCODE(0x2e, rol, 6, 0, ABSOLUTE,    &rol_fn)
    OPCODE(0x3e, rol, 7, 0, ABSOLUTE_X,  &rol_fn)
    OPCODE(0x6a, ror, 2, 0, ACCUMULATOR, &ror_fn)
    OPCODE(0x66, ror, 5, 0, ZERO_PAGE,   &ror_fn)
    OPCODE(0x76, ror, 6, 0, ZERO_PAGE_X, &ror_fn)
    OPCODE(0x6e, ror, 6, 0, ABSOLUTE,    &ror_fn)
    OPCODE(0x7e, ror, 7, 0, ABSOLUTE_X,  &ror_fn)
    OPCODE(0x40, rti, 6, 0, IMPLIED,     &rti_fn)
    OPCODE(0x60, rts, 6, 0, IMPLIED,     &rts_fn)
    OPCODE(0xe9, sbc, 2, 0, IMMEDIATE,   &sbc_fn)
    OPCODE(0xe5, sbc, 3, 0, ZERO_PAGE,   &sbc_fn)
    OPCODE(0xf5, sbc, 4, 0, ZERO_PAGE_X, &sbc_fn)
    OPCODE(0xed, sbc, 4, 0, ABSOLUTE,    &sbc_fn)
    OPCODE(0xfd, sbc, 4, 1, ABSOLUTE_X,  &sbc_fn)
    OPCODE(0xf9, sbc, 4, 1, ABSOLUTE_Y,  &sbc_fn)
    OPCODE(0xe1, sbc, 6, 0, INDIRECT_X,  &sbc_fn)
    OPCODE(0xf1, sbc, 5, 1, INDIRECT_Y,  &sbc_fn)
    OPCODE(0x38, sec, 2, 0, IMPLIED,     &sec_fn)
    OPCODE(0xf8, sed, 2, 0, IMPLIED,     &sed_fn)
    OPCODE(0x78, sei, 2, 0, IMPLIED,     &sei_fn)
    OPCODE(0x85, sta, 3, 0, ZERO_PAGE,   &sta_fn)
    OPCODE(0x95, sta, 4, 0, ZERO_PAGE_X, &sta_fn)
    OPCODE(0x8d, sta, 4, 0, ABSOLUTE,    &sta_fn)
    OPCODE(0x9d, sta, 5, 0, ABSOLUTE_X,  &sta_fn)
    OPCODE(0x99, sta, 5, 0, ABSOLUTE_Y,  &sta_fn)
    OPCODE(0x81, sta, 6, 0, INDIRECT_X,  &sta_fn)
    OPCODE(0x91, sta, 6, 0, INDIRECT_Y,  &sta_fn)
    OPCODE(0x86, stx, 3, 0, ZERO_PAGE,   &stx_fn)
    OPCODE(0x96, stx, 4, 0, ZERO_PAGE_Y, &stx_fn)
    OPCODE(0x8e, stx, 4, 0, ABSOLUTE,    &stx_fn)
    OPCODE(0x84, sty, 3, 0, ZERO_PAGE,   &sty_fn)
    OPCODE(0x94, sty, 4, 0, ZERO_PAGE_X, &sty_fn)
    OPCODE(0x8c, sty, 4, 0, ABSOLUTE,    &sty_fn)
    OPCODE(0xaa, tax, 2, 0, IMPLIED,     &tax_fn)
    OPCODE(0xa8, tay, 2, 0, IMPLIED,     &tay_fn)
    OPCODE(0xba, tsx, 2, 0, IMPLIED,     &tsx_fn)
    OPCODE(0x8a, txa, 2, 0, IMPLIED,     &txa_fn)
    OPCODE(0x9a, txs, 2, 0, IMPLIED,     &txs_fn)
    OPCODE(0x98, tya, 2, 0, IMPLIED,     &tya_fn)


    OPCODE(0x1a, nop, 2, 0, IMPLIED,     &nop_fn)
    OPCODE(0x3a, nop, 2, 0, IMPLIED,     &nop_fn)
    OPCODE(0x5a, nop, 2, 0, IMPLIED,     &nop_fn)
    OPCODE(0x7a, nop, 2, 0, IMPLIED,     &nop_fn)
    OPCODE(0xda, nop, 2, 0, IMPLIED,     &nop_fn)
    OPCODE(0xfa, nop, 2, 0, IMPLIED,     &nop_fn)
};

u8 opcode_len(u8 opcode)
{
    switch (opcode_list[opcode].mode) {
    case ABSOLUTE:
    case ABSOLUTE_X:
    case ABSOLUTE_Y:
    case INDIRECT:
        return 3;
    case ACCUMULATOR:
    case IMPLIED:
        return 1;
    case IMMEDIATE:
    case INDIRECT_X:
    case INDIRECT_Y:
    case RELATIVE:
    case ZERO_PAGE:
    case ZERO_PAGE_X:
    case ZERO_PAGE_Y:
        return 2;
    }
    return 1;
}

int opcode_addressing_mode(u8 opcode)
{
    int ret = IMPLIED;
    u8 column = opcode % 0x20;
    u8 row = opcode & 0xe0;
    /* exceptions */
    if (opcode == 0x20) return ABSOLUTE;
    if (opcode == 0x6c) return INDIRECT;
    /* generalized exceptions */
    if ((column == 0xa) && (row <= 0x60)) return ACCUMULATOR;
    if (((column == 0x0) || (column == 0x2)) && (row <= 0x60))
        return IMPLIED;
    if ((row == 0x80) || (row == 0xa0)) {
        if ((column == 0x1e) || (column == 0x1f))
            return ABSOLUTE_Y;
        if ((column == 0x16) || (column == 0x17))
            return ZERO_PAGE_Y;
    }

    /* standard */
    switch(column) {
    case 0x1:
    case 0x3:
        return INDIRECT_X;
    case 0x11:
    case 0x13:
        return INDIRECT_Y;
    case 0x4:
    case 0x5:
    case 0x6:
    case 0x7:
        return ZERO_PAGE;
    case 0x8:
    case 0xa:
    case 0x12:
    case 0x18:
    case 0x1a:
        return IMPLIED;
    case 0x0:
    case 0x2:
    case 0x9:
    case 0xb:
        return IMMEDIATE;
    case 0xc:
    case 0xd:
    case 0xe:
    case 0xf:
        return ABSOLUTE;
    case 0x10:
        return RELATIVE;
    case 0x14:
    case 0x15:
    case 0x16:
    case 0x17:
        return ZERO_PAGE_X;
    case 0x19:
    case 0x1b:
        return ABSOLUTE_Y;
    case 0x1c:
    case 0x1d:
    case 0x1e:
    case 0x1f:
        return ABSOLUTE_X;
    }
    return ret;
}

void check_modes(void) {
    int i;
    for (i = 0; i < 0xff; i++) {
        if (!opcode_list[i].op_func) {
            printf("OPCODE: %02x - UNDEFINED mode: %d\n", i, opcode_addressing_mode(i));
            opcode_list[i].op_func = &nop_fn;
            opcode_list[i].opcode = i;
            opcode_list[i].mode = opcode_addressing_mode(i);
            opcode_list[i].name = "nop(undef)";
            continue;
        }
        if (opcode_list[i].mode == opcode_addressing_mode(i))
            ;//printf("opcode: %02x matches\n", i);
        else
            printf("OPCODE: %02x DOES NOT MATCH %d != %d\n", i, opcode_list[i].mode, opcode_addressing_mode(i));
    }
}

void interrupt_request(void)
{
    if (MOS6502_state.SR.I)
        return;
    stack_push16(MOS6502_state.PC);
    stack_push8(MOS6502_state.SR.flags);
    MOS6502_state.SR.I = 1;
    MOS6502_state.PC = mem_load(IRQ_VEC_1) | (mem_load(IRQ_VEC_0) << 8);
}

void non_maskable_interrupt(void)
{
    stack_push16(MOS6502_state.PC);
    stack_push8(MOS6502_state.SR.flags);
    MOS6502_state.SR.I = 1;
    MOS6502_state.PC = mem_load(NMI_VEC_1) | (mem_load(NMI_VEC_0) << 8);
}

void fetch_op(void)
{
    int len;
    MOS6502_state.curr_opcode = mem_load(MOS6502_state.PC);
    MOS6502_state.curr_op = &opcode_list[MOS6502_state.curr_opcode];
    len = opcode_len(MOS6502_state.curr_opcode);
    MOS6502_state.curr_opcode_mode = MOS6502_state.curr_op->mode;
    if (len >= 2)
        MOS6502_state.curr_operand.par[0] = mem_load(MOS6502_state.PC + 1);
    if (len == 3)
        MOS6502_state.curr_operand.par[1] = mem_load(MOS6502_state.PC + 2);
    MOS6502_state.curr_op_length = len;
}

void init_cpu(void)
{
    MOS6502_state.A = 0;
    MOS6502_state.X = 0;
    MOS6502_state.Y = 0;
    MOS6502_state.SP = 0xfc;
    //MOS6502_state.SR.unused = 1;
    //MOS6502_state.SR.Z = 1;
    MOS6502_state.SR.flags = 0x20;
    MOS6502_state.PC = mem_load(RST_VEC_1) | (mem_load(RST_VEC_0) << 8);
    //MOS6502_state.PC = 0xc000;
    MOS6502_state.curr_op = &opcode_list[NOP];
    DUMP_STATE;
}

void idle_cpu(int cycles)
{
    MOS6502_state.cycles_inactive += cycles;
}

void cpu_cycle(void)
{
    if (MOS6502_state.cycles_inactive == 0)
    {
        fetch_op();
        CPU_DBG("%s : %02x %02x %02x %s\n", MOS6502_state.curr_op->name
                                    ,MOS6502_state.curr_opcode
                                    ,MOS6502_state.curr_operand.par[0]
                                    ,MOS6502_state.curr_operand.par[1]
                                    ,addr_modes[MOS6502_state.curr_opcode_mode]);
        (*addr_funcs[MOS6502_state.curr_opcode_mode])();
        /* execute */
        MOS6502_state.curr_op->op_func();
        MOS6502_state.cycles_inactive += MOS6502_state.curr_op->cycles;
        DUMP_STATE;
        MOS6502_state.PC += MOS6502_state.curr_op_length;
    }
    MOS6502_state.cycles_inactive--;
}
